package mvc;

import java.util.Scanner;

import mvc.controller.Controller;
import mvc.model.Model;
import mvc.view.View;

public class Client {
	public static void main(String[] args){
		Model model = new Model();
		View view = new View (model);
		Controller controller = new Controller(model, view);
		Scanner in = new Scanner(System.in);
		
		
		while (true){
			System.out.println("Dammi il comando");
			String comando = in.nextLine();
			view.input(comando);
		}
	}
}
