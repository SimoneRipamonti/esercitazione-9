package mvc.view;

import java.util.Observable;
import java.util.Observer;

import mvc.model.Model;
import mvc.model.State;

public class View extends Observable implements Observer {

	public View(Model gioco) {
		gioco.addObserver(this);
		System.out.println(gioco);
	}

	public void input(String input){
		this.setChanged();
		this.notifyObservers(input);
	}
	
	@Override
	public void update(Observable o, Object arg) {
		if (arg instanceof State) {
			State state = (State) arg;
			if (state.equals(State.WIN)) {
				System.out.println("Avete vinto");
			} else {
				System.out.println("Siete morti");
			}
			System.exit(0);
		} else {
			Model gioco = (Model) o;
			System.out.println(gioco);
		}
	}
}
