package mvc.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Observable;
import java.util.Set;

public class Model extends Observable {
	// insieme dei prigionieri
	private Set<Prigioniero> prigionieri;
	// insieme dei prigionieri gi� stati nella cella
	private Set<Prigioniero> prigionieriInTheRoom;
	// prigioniero attualmente nella cella
	private Prigioniero currentPrigioniero;
	// stato dello switch
	private Switch interruttore;
	// stato del gioco
	private State gameState;

	public Model() {
		prigionieri = new HashSet<Prigioniero>();
		prigionieriInTheRoom = new HashSet<Prigioniero>();
		this.setInterruttore(Switch.OFF);
		this.setGameState(State.RUNNING);
		this.prigionieri.add(new Prigioniero("Luca"));
		this.prigionieri.add(new Prigioniero("Paolo"));
		this.prigionieri.add(new Prigioniero("Pietro"));
		this.addPrigioniero(new Prigioniero("Luca"));
	}

	// aggiunge un prigioniero nella cella
	public void addPrigioniero(Prigioniero prigioniero) {
		this.currentPrigioniero = prigioniero;
	}

	public void changePrigioniero() {
		List<Prigioniero> prigionieriList = new ArrayList<Prigioniero>(
				prigionieri);
		Collections.shuffle(prigionieriList);
		this.currentPrigioniero = prigionieriList.get(0);
		this.prigionieriInTheRoom.add(currentPrigioniero);
		this.setChanged();
		this.notifyObservers(); // notifica mandata solo se prima � stato
								// mandato un setChaged()
	}

	public Switch getInterruttore() {
		return interruttore;
	}

	public void setInterruttore(Switch interruttore) {
		this.interruttore = interruttore;
		this.setChanged();
	}

	public State getGameState() {
		return gameState;
	}

	public void setGameState(State gameState) {
		this.gameState = gameState;
		this.setChanged();
	}

	@Override
	public String toString() {
		return "Model [currentPrigioniero=" + currentPrigioniero
				+ ", interruttore=" + interruttore + "]";
	}

	public Set<Prigioniero> getPrigionieri() {
		return prigionieri;
	}

	public void setPrigionieri(Set<Prigioniero> prigionieri) {
		this.prigionieri = prigionieri;
	}

	public Set<Prigioniero> getPrigionieriInTheRoom() {
		return prigionieriInTheRoom;
	}

	public void setPrigionieriInTheRoom(Set<Prigioniero> prigionieriInTheRoom) {
		this.prigionieriInTheRoom = prigionieriInTheRoom;
	}

	public Prigioniero getCurrentPrigioniero() {
		return currentPrigioniero;
	}

	public void setCurrentPrigioniero(Prigioniero currentPrigioniero) {
		this.currentPrigioniero = currentPrigioniero;
	}

}
