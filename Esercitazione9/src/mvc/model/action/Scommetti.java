package mvc.model.action;

import mvc.model.Model;
import mvc.model.State;

public class Scommetti extends Action {

	public Scommetti(Model gioco) {
		super(gioco);
	}

	@Override
	public void esegui() {
		if (this.getGioco().getPrigionieriInTheRoom()
				.containsAll(this.getGioco().getPrigionieri())) {
			this.getGioco().setGameState(State.WIN);
		}
		else{
			this.getGioco().setGameState(State.LOSE);
		}
	}

}
