package mvc.model.action;

import mvc.model.Model;
import mvc.model.Switch;

public class TurnOn extends Action {

	public TurnOn(Model gioco) {
		super(gioco);
	}

	@Override
	public void esegui() {
		this.getGioco().setInterruttore(Switch.ON);
		this.getGioco().changePrigioniero();
	}

}
