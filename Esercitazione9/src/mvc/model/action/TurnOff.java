package mvc.model.action;

import mvc.model.Model;
import mvc.model.Switch;

public class TurnOff extends Action {

	public TurnOff(Model gioco) {
		super(gioco);
	}

	@Override
	public void esegui() {
		this.getGioco().setInterruttore(Switch.OFF);
		this.getGioco().changePrigioniero();
	}

}
