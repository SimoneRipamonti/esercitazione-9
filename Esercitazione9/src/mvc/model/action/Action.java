package mvc.model.action;

import mvc.model.Model;

public abstract class Action {
	private final Model gioco;

	public Action(Model gioco) {
		this.gioco = gioco;
	}

	protected Model getGioco() {
		return gioco;
	}

	public abstract void esegui();
}
