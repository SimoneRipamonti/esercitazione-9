package mvc.controller;

import java.util.Observable;
import java.util.Observer;

import mvc.model.Model;
import mvc.model.Switch;
import mvc.model.action.Action;
import mvc.model.action.Scommetti;
import mvc.model.action.TurnOff;
import mvc.model.action.TurnOn;
import mvc.view.View;

public class Controller implements Observer {
	private final Model gioco;

	public Controller(Model gioco, View view) {
		this.gioco = gioco;
		view.addObserver(this);
	}

	@Override
	public void update(Observable o, Object arg) {
		Action action;
		if (arg.equals(Switch.ON.toString())) {
			action = new TurnOn(gioco);
		} else {
			if (arg.equals(Switch.OFF.toString())) {
				action = new TurnOff(gioco);
			} else {
				action = new Scommetti(gioco);
			}
		}
		action.esegui();
	}
}
